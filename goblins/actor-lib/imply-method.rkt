#lang racket/base

;;; Copyright 2019-2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(require "../core.rkt"
         "select-swear.rkt")

(provide ^imply-method)

(define (^imply-method bcom wrap-me method
                       [debug-name '^imply-method])
  (define $/<-
    (select-$/<- wrap-me))
  (procedure-rename
   (make-keyword-procedure
    (lambda (kws kw-args . args)
      (keyword-apply $/<- kws kw-args wrap-me method args)))
   debug-name))

(module+ test
  (require rackunit
           racket/match)
  (define am (make-actormap))
  (define (^foo-or-bar bcom)
    (match-lambda*
      [(list 'foo arg1 arg2)
       (list 'got-foo arg1 arg2)]
      [(list 'bar arg1 arg2)
       (list 'got-bar arg1 arg2)]))
  (define foo-or-bar (actormap-spawn! am ^foo-or-bar))
  (define fob-imp (actormap-spawn! am ^imply-method foo-or-bar 'foo))
  (check-equal?
   (actormap-peek am fob-imp 'beep 'boop)
   '(got-foo beep boop)))
