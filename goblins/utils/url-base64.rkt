#lang racket

;;; Copyright 2020 Christine Lemmer-Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(provide url-base64-encode url-base64-decode)

(require net/base64)

(define (url-base64-encode bytes)
  (string-replace
   (string-replace
    (string-trim
     (bytes->string/latin-1 (base64-encode bytes #""))
     "="
     #:left? #f
     #:repeat? #t)
    "+" "-")
   "/" "_"))

(define (url-base64-decode str)
  (base64-decode
   (string->bytes/latin-1
    (string-replace
     (string-replace str "-" "+")
     "_" "/"))))
